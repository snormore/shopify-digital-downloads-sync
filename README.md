Bulk sync your local images to Shopify as products with their Digital Downloads app.

# Getting started

 - On your Shopify store, make sure you the Shopify owned [Digital Downloads app](https://apps.shopify.com/digital-downloads) installed.
 - Install [Homebrew](https://brew.sh/) if necessary
 - Install Ruby with `brew install ruby`
 - Install geckodriver with `brew install geckodriver`
 - Install imagemagick with `brew install imagemagick`
 - Install bundler with `gem install bundler`
 - Clone this project with `git clone https://gitlab.com/snormore/shopify-digital-downloads-sync`
 - `cd shopify-digital-downloads-sync`
 - `bundle install` to install Ruby project dependencies

## Building and syncing your products

The location of your original images is defined in your `config.yml` file in the project. Variations of your images as for digital products are also defined in `config.yml`, including preview images with your given watermarks. See the default `config.yml.example` and the end of this README for examples.

```
bin/build
```

At this point here you will be prompted to import the generated product CSVs into your Shopify shop, a browser will open, click "Import" and import each batch of 100 that were rendered into your `config.yml` defined location. This batch size is recommended by Shopify, so there will be several CSV files if you are importing more than 100 images at the same time.

```
bin/sync
```

This will open a browser, login to your Shopify shop using the credentials you provide in `secrets.yml` (see `secrets.yml.example`), and upload each of your variants to Shopify's own Digital Downloads app. Kick it off and wait, it takes about 2 seconds per variant, so do the math on your number of products/variants.

----

`secrets.yml` contains your secrets for AWS (preview image URLs for Shopify products import) and your Shopify shop login details.

```yaml
aws:
  access_key_id: yourkey
  secret_access_key: yoursecret
shopify:
  shop_domain: yourshop.myshopify.com
  email: yourmemail
  password: yourpassword
```

----

`config.yml` contains your configuration for Shopify product and image generation from your original images.

```yaml
imports: data/imports
originals: data/images/original
product:
  vendor: Acme Photos
  type: 
  tag_keys:
    - category
    - copyright
variants:
  - directory: data/images/original
    title: Original Size ({width}x{height})
    price: 90.0
  - directory: data/images/presentation
    title: Presentation ({width}x{height})
    width: 640
    height: 480
    price: 50.0
previews:
  - directory: data/images/thumbnail
    width: 240
    height: 180
  - directory: data/images/presentation-watermarked
    width: 640
    height: 480
    watermark: data/images/watermark.png
```
