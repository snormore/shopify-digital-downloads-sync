require 'bundler/setup'
require 'selenium-webdriver'
require 'parallel'
require 'iptc'
require 'mini_magick'
require 'titleize'
require 'csv'
require 'aws-sdk'
require 'byebug'

class Builder
  def run
    output_dirs = [config[:imports]] + config[:variants].map { |r| r[:directory] unless original?(r[:directory]) }.compact + config[:previews].map { |r| r[:directory] }
    output_dirs.each do |dir|
      if Dir.exists?(dir) && !Dir["#{dir}/*"].empty?
        puts "It looks like you have some files in #{dir}, this needs to be cleared of old builds before starting a new one. You can run script/clean to do this."
        return
      end
    end

    puts "Processing images from #{config[:originals]}..."
    paths = Dir["#{config[:originals]}/**/*"].map do |filename|
      filename if File.file?(filename) && filename.end_with?('.jpg')
    end.compact
    i = 0
    import_files = paths.each_slice(100).map do |batch|
      products = process_images(batch)
      import_file = render_csv(products, i)
      i += 1
      import_file
    end

    puts
    puts "Go to the browser window that just opened, and import the following product files"
    import_files.each do |import_file|
      puts "  #{File.join(Dir.pwd, import_file)}"
    end
    puts "After you've imported all product CSVs, run bin/sync to load the images to Shopify as Digital Downloads."

    browser.navigate.to(shop_admin_url('products'))
    login
    input = wait.until {
      element = browser.find_element(id: 'csv_input_field')
      element if element.displayed?
    }
    input.send_keys(File.join(Dir.pwd, import_files.first))
  end

  private

    def original?(dir)
      dir == config[:originals]
    end

    def render_csv(products, batch_index)
      # Render product CSV for import
      empty_product = {
        'Handle' => nil,
        'Title' => nil,
        'Body (HTML)' => nil,
        'Vendor' => nil,
        'Type' => nil,
        'Tags' => nil,
        'Published' => nil,
        'Option1 Name' => nil,
        'Option1 Value' => nil,
        'Option2 Name' => nil,
        'Option2 Value' => nil,
        'Option3 Name' => nil,
        'Option3 Value' => nil,
        'Variant SKU' => nil,
        'Variant Grams' => nil,
        'Variant Inventory Tracker' => nil,
        'Variant Inventory Qty' => 0,
        'Variant Inventory Policy' => 'continue',
        'Variant Fulfillment Service' => 'manual',
        'Variant Price' => nil,
        'Variant Compare At Price' => nil,
        'Variant Requires Shipping' => 'FALSE',
        'Variant Taxable' => 'TRUE',
        'Variant Barcode' => nil,
        'Image Src' => nil,
        'Image Alt Text' => nil,
        'Gift Card' => nil,
        'Google Shopping / MPN' => nil,
        'Google Shopping / Age Group' => nil,
        'Google Shopping / Gender' => nil,
        'Google Shopping / Google Product Category' => nil,
        'SEO Title' => nil,
        'SEO Description' => nil,
        'Google Shopping / AdWords Grouping' => nil,
        'Google Shopping / AdWords Labels' => nil,
        'Google Shopping / Condition' => nil,
        'Google Shopping / Custom Product' => nil,
        'Google Shopping / Custom Label 0' => nil,
        'Google Shopping / Custom Label 1' => nil,
        'Google Shopping / Custom Label 2' => nil,
        'Google Shopping / Custom Label 3' => nil,
        'Google Shopping / Custom Label 4' => nil,
        'Variant Image' => nil,
        'Variant Weight Unit' => nil
      }
      products = products.map do |product|
        category_tags = []
        keyword_tags = []
        product[:tags].split(',').each do |tag|
          parts = tag.strip.split(' ', 2)
          parts = tag.strip.split('_', 2) if parts.size == 1
          category_tags << parts.last if parts.first == 'category'
          keyword_tags << parts.last if parts.first == 'keyword'
        end
        first_product_row_base = empty_product.merge({
          'Handle' => product[:handle],
          'Title' => product[:title],
          'Body (HTML)' => product[:description],
          'Vendor' => config[:product][:vendor],
          'Type' => config[:product][:type],
          'Tags' => product[:tags],
          'SEO Title' => config[:product][:seo][:title].gsub('{product_title}', product[:title]),
          'SEO Description' => config[:product][:seo][:description].gsub('{product_title}', product[:title])
                                                                   .gsub('{category_tags}', category_tags.join(' & '))
                                                                   .gsub('{keyword_tags}', keyword_tags.join(', ')),
        })
        config[:variants].map.each_with_index do |variant, i|
          image_url = i < product[:images].size ? product[:images][i] : nil
          base_row = i == 0 ? first_product_row_base : empty_product
          variant_image = MiniMagick::Image.open(File.join(variant[:directory], "#{product[:handle]}.jpg"))
          base_row.merge({
            'Handle' => product[:handle],
            'Option1 Name' => 'Title',
            'Option1 Value' => variant[:title].gsub('{width}', variant_image.width.to_s)
                                              .gsub('{height}', variant_image.height.to_s),
            'Variant Price' => variant[:price],
            'Variant SKU' => variant[:handle],
            'Image Src' => image_url,
            'Image Alt Text' => product[:description],
          })
        end
      end.flatten
      begin
        Dir.mkdir(config[:imports]) unless Dir.exists?(config[:imports])
      rescue Errno::EEXIST
      end
      output_file = File.join(config[:imports], "products-#{batch_index.to_s.rjust(4, '0')}.csv")
      CSV.open(output_file, 'wb') do |csv|
          csv << products.first.keys
          products.each do |product|
            csv << product.values
          end
      end
      output_file
    end

    def process_images(paths)
      # paths.map { |path| process_image(path) }
      Parallel.map(paths) { |path| process_image(path) }
    end

    def build_tags_from_markers(value)
      value.map { |value| value.split('Tag=').last.strip.split(',') if value.include?('Tag=') }.compact.flatten
    end

    def build_tags_and_keywords(values)
      values = [values] if values.is_a?(String)
      tags = []
      keywords = []
      values.each do |value|
        key = value.split('_').first
        if config[:product][:tag_keys].include?(key) || config[:product][:tag_keys].include?("@#{key}")
          tags << value
        else
          keywords << "keyword #{value}"
        end
      end
      [tags, keywords]
    end

    def process_image(original_image_path)
      handle = original_image_path.downcase
      extension = File.extname(handle)
      handle = File.basename(handle, extension)

      # Read metadata from image
      data = IPTC::JPEG::Image.from_file(original_image_path)
      tags, keywords = build_tags_and_keywords(data.values['iptc/Keywords']&.value)
      if data.values['iptc/Unknown marker']
        tags += build_tags_from_markers(data.values['iptc/Unknown marker']&.value)
      end
      tags = tags.map { |tag| tag.strip.gsub('_', ' ') }.uniq
      tags += keywords
      product = {
        handle: handle,
        title: handle.gsub('-', ' ').titleize,
        description: data.values['iptc/Caption']&.value,
        tags: tags.join(',')
      }
      print('.')

      # Build variant images
      config[:variants].each do |variant|
        next if original?(variant[:directory])
        img = MiniMagick::Image.open(original_image_path)
        img.resize("#{variant[:width]}x#{variant[:height]}")
        begin
          Dir.mkdir(variant[:directory]) unless Dir.exists?(variant[:directory])
        rescue Errno::EEXIST
        end
        img.write(File.join(variant[:directory], "#{handle}.jpg"))
      end

      # Build preview images, with watermark
      product[:images] = config[:previews].map do |preview|
        img = MiniMagick::Image.open(original_image_path)
        img.strip
        img.resize("#{preview[:width]}x#{preview[:height]}")
        unless preview[:watermark].nil?
          img.combine_options do |c|
            c.gravity preview[:watermark_position] || 'SouthWest'
            c.draw "image Over 0,0 0,0 \"#{preview[:watermark]}\""
          end
        end
        begin
          Dir.mkdir(preview[:directory]) unless Dir.exists?(preview[:directory])
        rescue Errno::EEXIST
        end
        preview_path = File.join(preview[:directory], "#{handle}.jpg")
        img.write(preview_path)
        published_preview_image_url(preview_path)
      end

      product
    end

    def published_preview_image_url(path)
      bucket = s3.bucket('shopify-digital-downloads-sync-public-previews')
      extension = File.extname(path)
      obj = bucket.object(File.join(Time.now.utc.strftime('%Y%m%d'), "#{SecureRandom.uuid}#{extension}"))
      obj.upload_file(path, acl: 'public-read')
      obj.public_url
    end

    def s3
      @s3 ||= Aws::S3::Resource.new(client: Aws::S3::Client.new(
        region: 'us-east-1',
        access_key_id: secrets[:aws][:access_key_id],
        secret_access_key: secrets[:aws][:secret_access_key]
      ))
    end

    def config
      @config ||= symbolize_keys(YAML.load_file('config.yml'))
    end

    def secrets
      @secrets ||= symbolize_keys(YAML.load_file('secrets.yml'))
    end

    def symbolize_keys(hash)
      JSON.parse(JSON.dump(hash), symbolize_names: true)
    end

    def shop_admin_url(path='')
      url = "https://#{secrets[:shopify][:shop_domain]}/admin"
      url = File.join(url, path) if path
      url
    end

    def wait
      @wait ||= Selenium::WebDriver::Wait.new(timeout: 60)
    end

    def browser
      profile = Selenium::WebDriver::Firefox::Profile.new
      profile['dom.file.createInChild'] = true
      options = Selenium::WebDriver::Firefox::Options.new(profile: profile)
      @browser ||= Selenium::WebDriver.for(:firefox, options: options)
    end

    def login
      input = wait.until {
        element = browser.find_element(:name, 'login')
        element if element.displayed?
      }
      input.send_keys(secrets[:shopify][:email])
      input = wait.until {
        element = browser.find_element(:name, 'password')
        element if element.displayed?
      }
      input.send_keys(secrets[:shopify][:password])
      form = wait.until {
        element = browser.find_element(:tag_name, 'form')
        element if element.displayed?
      }
      form.find_element(:name, 'commit').click
    end
end

if __FILE__ == $0
  Builder.new.run
end
