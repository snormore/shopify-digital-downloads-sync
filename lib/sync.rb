require 'selenium-webdriver'
require 'yaml'
require 'csv'
require 'byebug'

class Syncer

  def run
    browser.navigate.to(shop_admin_url)
    login
    variant_configs_by_title = config[:variants].inject({}) { |h, variant| h[variant[:title].downcase] = variant; h }
    first = true
    shopify_products.each do |product|
      browser.navigate.to("#{shop_admin_url}/apps/digital-downloads/login/fwd_product?id=#{product[:id]}&shop=#{secrets[:shopify][:shop_domain]}")
      wait_for_app(first: first)
      first = false
      product[:variants].each_with_index do |variant, i|
        puts "#{Time.now.utc.to_i} #{product[:title]} / #{variant[:title]}"

        # Upload image as digital product
        form = wait.until {
          element = browser.find_element(css: "form[data-variant-id='#{variant[:id]}']")
          element if element.displayed?
        }
        variant[:config] = variant_configs_by_title[variant[:title].downcase]
        if variant[:config].nil?
          # It was a dynamic title, which is only supported on the first, the original
          variant[:config] = config[:variants].first
        end
        input = form.find_element(css: "input[type='file']")
        input.send_keys(File.join(Dir.pwd, "#{variant[:config][:directory]}/#{product[:handle]}.jpg"))
        upload_wait.until {
          successes = browser.find_elements(css: '.delivery-product-variant-upload-success')
          successes.size == i + 1
        }
        sleep 1
      end
    end
    browser.navigate.to(shop_admin_url('products'))
    # browser.quit
  end

  private

    def config
      @config ||= symbolize_keys(YAML.load_file('config.yml'))
    end

    def secrets
      @secrets ||= symbolize_keys(YAML.load_file('secrets.yml'))
    end

    def symbolize_keys(hash)
      JSON.parse(JSON.dump(hash), symbolize_names: true)
    end

    def shop_admin_url(path='')
      url = "https://#{secrets[:shopify][:shop_domain]}/admin"
      url = File.join(url, path) if path
      url
    end

    def wait
      @wait ||= Selenium::WebDriver::Wait.new(timeout: 60)
    end

    def upload_wait
      @upload_wait ||= Selenium::WebDriver::Wait.new(timeout: 120)
    end

    def browser
      profile = Selenium::WebDriver::Firefox::Profile.new
      profile['dom.file.createInChild'] = true
      options = Selenium::WebDriver::Firefox::Options.new(profile: profile)
      @browser ||= Selenium::WebDriver.for(:firefox, options: options)
    end

    def login
      input = wait.until {
        element = browser.find_element(:name, 'login')
        element if element.displayed?
      }
      input.send_keys(secrets[:shopify][:email])
      input = wait.until {
        element = browser.find_element(:name, 'password')
        element if element.displayed?
      }
      input.send_keys(secrets[:shopify][:password])
      form = wait.until {
        element = browser.find_element(:tag_name, 'form')
        element if element.displayed?
      }
      form.find_element(:name, 'commit').click
    end

    def shopify_products
      Dir["#{config[:imports]}/*"].map do |import_file|
        rows = CSV.read(import_file)
        keys = rows.shift
        products = rows.map { |values| Hash[ keys.zip(values) ] }
        handles = products.map { |product| product['Handle'] }
        handles = handles.map.each_with_index { |handle, i| handle || handles[i - 1] }
        products = []
        handles.each_slice(20) do |batch|
          browser.navigate.to("view-source:https://#{secrets[:shopify][:shop_domain]}/admin/products.json?handle=#{batch.join(',')}")
          pre = wait.until {
            element = browser.find_element(tag_name: 'pre')
            element if element.displayed?
          }
          products += JSON.parse(pre.text, symbolize_names: true)[:products]
        end
        products
      end.flatten
    end

    def wait_for_app(first: false)
      # Wait for the app iframe and switch to it.
      app = wait.until {
        element = browser.find_element(id: 'EmbeddedApp')
        element if element.displayed?
      }
      app = wait.until {
        element = browser.find_element(name: 'app-iframe')
        element if element.displayed?
      }
      sleep 5 if first
      app = wait.until {
        element = browser.find_element(name: 'app-iframe')
        element if element.displayed?
      }
      browser.switch_to.frame(app)
    end
end

if __FILE__ == $0
  Syncer.new.run
end
